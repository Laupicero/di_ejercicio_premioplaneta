﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PremioPlaneta
{
    /// <summary>
    /// Clase que nos ayudará a crearnos las instancias de tipo 'Libro' para el array
    /// </summary>
    class Libro
    {
        private int anno;
        private String puesto; // Solo ganador o finalista
        private String autor;
        private String titulo;
        private String sipnosis;
        //Hace referencia al nombre de la imagen. se busca con ella el nombre de imagen en resources, para que así sea más dinámico
        private String nombrePortada; 


        //Constructores
        public Libro() { }
        public Libro(int anno, string puesto, string autor, string titulo, string sipnosis, string nombrePortada)
        {
            this.anno = anno;
            this.puesto = puesto;
            this.autor = autor;
            this.titulo = titulo;
            this.sipnosis = sipnosis;
            this.nombrePortada = nombrePortada;
        }


        //Setter y Getter
        public int ANNO { get => anno; set => anno = value; }
        public string PUESTO { get => puesto; set => puesto = value; }
        public string AUTOR { get => autor; set => autor = value; }
        public string TITULO { get => titulo; set => titulo = value; }
        public string SIPNOSIS { get => sipnosis; set => sipnosis = value; }
        public string NOMBREPORTADA { get => nombrePortada; set => nombrePortada = value; }
    }
}
