﻿
namespace PremioPlaneta
{
    partial class PremioPlanetaFormPrincipal
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelanno = new System.Windows.Forms.Label();
            this.cb_Anno = new System.Windows.Forms.ComboBox();
            this.tcNovelas = new System.Windows.Forms.TabControl();
            this.tpNovelaGanadora = new System.Windows.Forms.TabPage();
            this.tpNovelaFinalista = new System.Windows.Forms.TabPage();
            this.textBoxSipnosisFinalista = new System.Windows.Forms.TextBox();
            this.textBoxSipnosisGanador = new System.Windows.Forms.TextBox();
            this.pictureBoxGanador = new System.Windows.Forms.PictureBox();
            this.pictureBoxFinalista = new System.Windows.Forms.PictureBox();
            this.pictureBoxBolaPremioPlaneta = new System.Windows.Forms.PictureBox();
            this.lbAutorGanador = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbTituloGanador = new System.Windows.Forms.Label();
            this.lbTituloFinalista = new System.Windows.Forms.Label();
            this.lbAutorFinalista = new System.Windows.Forms.Label();
            this.tcNovelas.SuspendLayout();
            this.tpNovelaGanadora.SuspendLayout();
            this.tpNovelaFinalista.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGanador)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFinalista)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBolaPremioPlaneta)).BeginInit();
            this.SuspendLayout();
            // 
            // labelanno
            // 
            this.labelanno.AutoSize = true;
            this.labelanno.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelanno.Location = new System.Drawing.Point(48, 84);
            this.labelanno.Name = "labelanno";
            this.labelanno.Size = new System.Drawing.Size(45, 19);
            this.labelanno.TabIndex = 1;
            this.labelanno.Text = "AÑO";
            // 
            // cb_Anno
            // 
            this.cb_Anno.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cb_Anno.FormattingEnabled = true;
            this.cb_Anno.Location = new System.Drawing.Point(105, 76);
            this.cb_Anno.Name = "cb_Anno";
            this.cb_Anno.Size = new System.Drawing.Size(175, 28);
            this.cb_Anno.TabIndex = 2;
            this.cb_Anno.SelectedIndexChanged += new System.EventHandler(this.cb_Anno_SelectedIndexChanged);
            // 
            // tcNovelas
            // 
            this.tcNovelas.Controls.Add(this.tpNovelaGanadora);
            this.tcNovelas.Controls.Add(this.tpNovelaFinalista);
            this.tcNovelas.Location = new System.Drawing.Point(12, 189);
            this.tcNovelas.Name = "tcNovelas";
            this.tcNovelas.SelectedIndex = 0;
            this.tcNovelas.Size = new System.Drawing.Size(806, 397);
            this.tcNovelas.TabIndex = 3;
            // 
            // tpNovelaGanadora
            // 
            this.tpNovelaGanadora.Controls.Add(this.lbTituloGanador);
            this.tpNovelaGanadora.Controls.Add(this.label1);
            this.tpNovelaGanadora.Controls.Add(this.lbAutorGanador);
            this.tpNovelaGanadora.Controls.Add(this.pictureBoxGanador);
            this.tpNovelaGanadora.Controls.Add(this.textBoxSipnosisGanador);
            this.tpNovelaGanadora.Location = new System.Drawing.Point(4, 22);
            this.tpNovelaGanadora.Name = "tpNovelaGanadora";
            this.tpNovelaGanadora.Padding = new System.Windows.Forms.Padding(3);
            this.tpNovelaGanadora.Size = new System.Drawing.Size(798, 371);
            this.tpNovelaGanadora.TabIndex = 0;
            this.tpNovelaGanadora.Text = "NOVELA GANADORA";
            this.tpNovelaGanadora.UseVisualStyleBackColor = true;
            // 
            // tpNovelaFinalista
            // 
            this.tpNovelaFinalista.Controls.Add(this.lbTituloFinalista);
            this.tpNovelaFinalista.Controls.Add(this.lbAutorFinalista);
            this.tpNovelaFinalista.Controls.Add(this.pictureBoxFinalista);
            this.tpNovelaFinalista.Controls.Add(this.textBoxSipnosisFinalista);
            this.tpNovelaFinalista.Location = new System.Drawing.Point(4, 22);
            this.tpNovelaFinalista.Name = "tpNovelaFinalista";
            this.tpNovelaFinalista.Padding = new System.Windows.Forms.Padding(3);
            this.tpNovelaFinalista.Size = new System.Drawing.Size(798, 371);
            this.tpNovelaFinalista.TabIndex = 1;
            this.tpNovelaFinalista.Text = "NOVELA FINALISTA";
            this.tpNovelaFinalista.UseVisualStyleBackColor = true;
            // 
            // textBoxSipnosisFinalista
            // 
            this.textBoxSipnosisFinalista.Location = new System.Drawing.Point(253, 89);
            this.textBoxSipnosisFinalista.Multiline = true;
            this.textBoxSipnosisFinalista.Name = "textBoxSipnosisFinalista";
            this.textBoxSipnosisFinalista.Size = new System.Drawing.Size(530, 246);
            this.textBoxSipnosisFinalista.TabIndex = 0;
            // 
            // textBoxSipnosisGanador
            // 
            this.textBoxSipnosisGanador.Location = new System.Drawing.Point(255, 93);
            this.textBoxSipnosisGanador.Multiline = true;
            this.textBoxSipnosisGanador.Name = "textBoxSipnosisGanador";
            this.textBoxSipnosisGanador.Size = new System.Drawing.Size(517, 248);
            this.textBoxSipnosisGanador.TabIndex = 2;
            // 
            // pictureBoxGanador
            // 
            this.pictureBoxGanador.Image = global::PremioPlaneta.Properties.Resources.HombresDesnudos;
            this.pictureBoxGanador.Location = new System.Drawing.Point(25, 67);
            this.pictureBoxGanador.Name = "pictureBoxGanador";
            this.pictureBoxGanador.Size = new System.Drawing.Size(184, 279);
            this.pictureBoxGanador.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxGanador.TabIndex = 3;
            this.pictureBoxGanador.TabStop = false;
            // 
            // pictureBoxFinalista
            // 
            this.pictureBoxFinalista.Image = global::PremioPlaneta.Properties.Resources.LAIslaDeAlice;
            this.pictureBoxFinalista.Location = new System.Drawing.Point(21, 58);
            this.pictureBoxFinalista.Name = "pictureBoxFinalista";
            this.pictureBoxFinalista.Size = new System.Drawing.Size(189, 291);
            this.pictureBoxFinalista.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFinalista.TabIndex = 1;
            this.pictureBoxFinalista.TabStop = false;
            // 
            // pictureBoxBolaPremioPlaneta
            // 
            this.pictureBoxBolaPremioPlaneta.Image = global::PremioPlaneta.Properties.Resources.banner_nadal1;
            this.pictureBoxBolaPremioPlaneta.Location = new System.Drawing.Point(523, 12);
            this.pictureBoxBolaPremioPlaneta.Name = "pictureBoxBolaPremioPlaneta";
            this.pictureBoxBolaPremioPlaneta.Size = new System.Drawing.Size(291, 161);
            this.pictureBoxBolaPremioPlaneta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxBolaPremioPlaneta.TabIndex = 0;
            this.pictureBoxBolaPremioPlaneta.TabStop = false;
            // 
            // lbAutorGanador
            // 
            this.lbAutorGanador.AutoSize = true;
            this.lbAutorGanador.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAutorGanador.Location = new System.Drawing.Point(264, 67);
            this.lbAutorGanador.Name = "lbAutorGanador";
            this.lbAutorGanador.Size = new System.Drawing.Size(0, 19);
            this.lbAutorGanador.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(399, 174);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 22);
            this.label1.TabIndex = 5;
            // 
            // lbTituloGanador
            // 
            this.lbTituloGanador.AutoSize = true;
            this.lbTituloGanador.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTituloGanador.Location = new System.Drawing.Point(264, 21);
            this.lbTituloGanador.Name = "lbTituloGanador";
            this.lbTituloGanador.Size = new System.Drawing.Size(0, 22);
            this.lbTituloGanador.TabIndex = 5;
            // 
            // lbTituloFinalista
            // 
            this.lbTituloFinalista.AutoSize = true;
            this.lbTituloFinalista.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTituloFinalista.Location = new System.Drawing.Point(264, 17);
            this.lbTituloFinalista.Name = "lbTituloFinalista";
            this.lbTituloFinalista.Size = new System.Drawing.Size(0, 22);
            this.lbTituloFinalista.TabIndex = 7;
            // 
            // lbAutorFinalista
            // 
            this.lbAutorFinalista.AutoSize = true;
            this.lbAutorFinalista.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAutorFinalista.Location = new System.Drawing.Point(264, 58);
            this.lbAutorFinalista.Name = "lbAutorFinalista";
            this.lbAutorFinalista.Size = new System.Drawing.Size(0, 19);
            this.lbAutorFinalista.TabIndex = 6;
            // 
            // PremioPlanetaFormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(830, 598);
            this.Controls.Add(this.tcNovelas);
            this.Controls.Add(this.cb_Anno);
            this.Controls.Add(this.labelanno);
            this.Controls.Add(this.pictureBoxBolaPremioPlaneta);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "PremioPlanetaFormPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PREMIOS PLANETA";
            this.tcNovelas.ResumeLayout(false);
            this.tpNovelaGanadora.ResumeLayout(false);
            this.tpNovelaGanadora.PerformLayout();
            this.tpNovelaFinalista.ResumeLayout(false);
            this.tpNovelaFinalista.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxGanador)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFinalista)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxBolaPremioPlaneta)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxBolaPremioPlaneta;
        private System.Windows.Forms.Label labelanno;
        private System.Windows.Forms.ComboBox cb_Anno;
        private System.Windows.Forms.TabControl tcNovelas;
        private System.Windows.Forms.TabPage tpNovelaGanadora;
        private System.Windows.Forms.TabPage tpNovelaFinalista;
        private System.Windows.Forms.PictureBox pictureBoxFinalista;
        private System.Windows.Forms.TextBox textBoxSipnosisFinalista;
        private System.Windows.Forms.PictureBox pictureBoxGanador;
        private System.Windows.Forms.TextBox textBoxSipnosisGanador;
        private System.Windows.Forms.Label lbTituloGanador;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbAutorGanador;
        private System.Windows.Forms.Label lbTituloFinalista;
        private System.Windows.Forms.Label lbAutorFinalista;
    }
}

